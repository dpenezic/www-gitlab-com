---
layout: markdown_page
title: "Support and Development Process"
---

## Types of issues

* Bug

* Unpaid feature request

* Paid feature request

## Bug process

* Make sure it looks like bug - otherwise ping one of the developers to confirm.

* Reproduce bug

* Assign next milestone  (current 1.0 then assign 1.1)

* Report back to customer

## Unpaid feature request process

* Provide maximum information and use case from customer in the issue,
mention any alternatives, how badly the customer wants it.

* Ask a developer for opinion (do we want this feature in GitLab: yes/no, the developer might ask more questions before answer)

## Paid feature request process

* Points 1 and 2 of the Unpaid feature request process

* Ask for an estimate
